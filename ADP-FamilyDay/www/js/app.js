angular.module('ionicApp', ['ionic'])

  .controller('AppCtrl', function () {

    ionic.Platform.ready(function () {
      // navigator.splashscreen.hide();
    });

  });

var navigateToHome = function () {
  window.location = "../html/navigation.html";
}
var doLogout = function () {

}

var addCommonEventListener = function () {
  var element = document.getElementById("home");
  if (element)
    element.addEventListener("click", navigateToHome);

  element = document.getElementById("logout");
  if (element)
    element.addEventListener("click", doLogout);
}

var init = function () {
  addCommonEventListener();
}

document.addEventListener("DOMContentLoaded", function (event) {
  console.log("DOM fully loaded and parsed");
  init();
});

