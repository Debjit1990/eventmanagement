(function() {
    "use strict";

    var navigateToNotification = function() {
        window.location = "../html/notifications.html";
    }
    var navigateToEventDirection = function() {
        window.location = "../html/eventdirection.html";
    }
    var navigateToTrackFamily = function() {
        window.location = "../html/trackfamily.html";
    }
    var navigateToFindFriends = function() {
        window.location = "../html/findfriends.html";
    }
    var navigateToAddFamily = function() {
        window.location = "../html/addfamily.html";
    }
    var navigateToBroadcast = function() {
        window.location = "../html/notifications.html";
    }
    var navigateToContactUs = function() {
        window.location = "../html/contactus.html";
    }

    var addAllEventListener = function() {
        var element = document.getElementById("nav_notification");
        element.addEventListener("click", navigateToNotification);

        element = document.getElementById("nav_evt_direction");
        element.addEventListener("click", navigateToEventDirection);

        element = document.getElementById("nav_track_family");
        element.addEventListener("click", navigateToTrackFamily);

        element = document.getElementById("nav_find_frnds");
        element.addEventListener("click", navigateToFindFriends);

        element = document.getElementById("nav_add_family");
        element.addEventListener("click", navigateToAddFamily);

        element = document.getElementById("nav_broadcast");
        element.addEventListener("click", navigateToBroadcast);

        element = document.getElementById("nav_contact_us");
        element.addEventListener("click", navigateToContactUs);
    }

    var init = function() {
        addAllEventListener();
    }

    document.addEventListener("DOMContentLoaded", function(event) {
        console.log("DOM fully loaded and parsed");
        init();
    });

})();