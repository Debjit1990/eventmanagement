(function () {
    "use strict";

    // all defaults and constants
    var DATE_FORMAT_DEFAULT = "DD MMM YYYY  H:mm";
    var INTERVAL_DEFAULT = 5000;
    var EMPTY_STRING = "";
    var ASSOCIATE_FAMILY = "associate_family";
    var ASSOCIATE = "associate";

    var liStyle = {
        backgroundColor: "#EDEDED",
        opacity: 30,
        textAlign: "left",
        borderRadius: "4px",
        margin: "0 0 10px 0"
    };
    var notiContainerStyle = {
        padding: "7px"
    };
    var notiDateStyle = {
        color: "#0F5CB6",
        fontSize: "18px"
    };

    var notiMessageStyle = {
        color: "#4A4A4A",
        fontWeight: "lighter",
        fontSize: "24px"
    };

    var createDOMEleemnt = function (type, style) {
        if (typeof type === 'string') {
            var DOMElement = document.createElement(type);
            if (style) {
                _.extend(DOMElement.style, style);
            }
            return DOMElement;
        }
    }

    var dateToString = function (date, format) {
        if (date) {
            if (!format) {
                format = DATE_FORMAT_DEFAULT;
            }
            return moment(date).format(format);
        }
    }

    var createNotificationItem = function (notification) {
        var liElement, notiContainerElement, notiDateElement, notiMessageElement;
        var style;

        if (notification.unread) {
            style = _.extend({}, notiDateStyle);
            style.fontWeight = "bold";
        } else {
            style = notiDateStyle;
        }
        debugger;
        liElement = createDOMEleemnt("li", liStyle);
        notiContainerElement = createDOMEleemnt("div", notiContainerStyle);
        notiDateElement = createDOMEleemnt("div", style);
        notiMessageElement = createDOMEleemnt("div", notiMessageStyle);

        notiDateElement.innerHTML = dateToString(notification.dateTime);
        notiMessageElement.innerHTML = notification.message || '';

        notiContainerElement.appendChild(notiDateElement);
        notiContainerElement.appendChild(notiMessageElement);
        liElement.appendChild(notiContainerElement);
        return liElement;
    }

    var showNotificationList = function (notifications) {
        var notiItem, style;
        var notiListElement = document.getElementById("notification_list");

        for (var i = 0; i < notifications.length; i++) {
            notiItem = createNotificationItem(notifications[i]);
            notiListElement.appendChild(notiItem);

        }
    }

    var addToNotificationList = function (notifications) {
        debugger;
        var notiItem, style;
        if (!Array.isArray(notifications)) {
            notifications = [notifications];
        }
        var notiListElement = document.getElementById("notification_list");

        for (var j = notifications.length - 1; j >= 0; j--) {
            notiItem = createNotificationItem(notifications[j]);
            notiListElement.insertBefore(notiItem, notiListElement.childNodes[0]);
        }
    }

    var getMessage = function () {
        var sysdate = new Date();
        var optionsEl = document.getElementById("notification_options");
        var textEl = document.getElementById("notification_message");
        return {
            sendTo: optionsEl.value,
            message: textEl.value,
            dateTime: sysdate
        };
    }

    var clearMessage = function () {
        var optionsEl = document.getElementById("notification_options");
        var textEl = document.getElementById("notification_message");
        optionsEl.value = ASSOCIATE_FAMILY;
        textEl.value = EMPTY_STRING;
    }

    var enableNotificatonPost = function () {
        var row_notification_options = document.getElementById("row_notification_options");
        var row_notification_message = document.getElementById("row_notification_message");
        var row_notification_btn = document.getElementById("row_notification_btn");

        row_notification_options.setAttribute("style", "display: block;");
        row_notification_message.setAttribute("style", "display: block;");
        row_notification_btn.setAttribute("style", "display: block;");

        var notification_btn = document.getElementById("notification_btn");
        notification_btn.addEventListener("click", notification.onBroadcast);
    }

    var disableNotificatonPost = function () {
        var row_notification_options = document.getElementById("row_notification_options");
        var row_notification_message = document.getElementById("row_notification_message");
        var row_notification_btn = document.getElementById("row_notification_btn");

        row_notification_options.setAttribute("style", "display: none;");
        row_notification_message.setAttribute("style", "display: none;");
        row_notification_btn.setAttribute("style", "display: none;");

        var notification_btn = document.getElementById("notification_btn");
        notification_btn.removeEventListener("click", notification.onBroadcast);
    }

    // fetch user detail for the logged in user
    var getUserDetails = function () {
        return { user: "pulak", role: "admin" };
    }

    // pull all notificatons from server
    var pullNotifications = function () {
        var sysdate = new Date();
        return [
            {
                unread: true,
                message: "Notification text will appear here Notification text will appear here Notification text will...",
                dateTime: sysdate
            },
            {
                unread: true,
                message: "Notification text will appear here Notification text will appear here Notification text will...",
                dateTime: sysdate
            },
            {
                unread: true,
                message: "Notification text will appear here Notification text will appear here Notification text will...",
                dateTime: sysdate
            },
            {
                unread: false,
                message: "Notification text will appear here Notification text will appear here Notification text will...",
                dateTime: sysdate
            },
            {
                unread: false,
                message: "Notification text will appear here Notification text will appear here Notification text will...",
                dateTime: sysdate
            },
            {
                unread: false,
                message: "Notification text will appear here Notification text will appear here Notification text will...",
                dateTime: sysdate
            }
        ];
    }

    // notification will be broadcasted here
    var pushNotification = function () {
        debugger;
        var notification = getMessage();
        if (notification && notification.message) {
            addToNotificationList({
                dateTime: notification.dateTime,
                message: notification.message,
                unread: true
            });
            clearMessage();
        }
    }

    // every 5seconds this will try to fetch new notifications form server
    var checkForNewNotifications = function () {
        debugger;
        var newNotifications = pullNotifications();
        if (newNotifications && newNotifications.length > 0) {
            addToNotificationList(newNotifications);
        }
    };

    // this function initialize notification page when landed
    var init = function () {
        debugger;
        var user = getUserDetails();
        if (user && (user.role === "admin" || user.role === "hr")) {
            enableNotificatonPost();
        } else {
            disableNotificatonPost();
        }

        var notifications = pullNotifications();
        showNotificationList(notifications);

        setInterval(checkForNewNotifications, INTERVAL_DEFAULT);
    }

    var notification = {
        onBroadcast: function () {
            pushNotification();
        }
    }

    document.addEventListener("DOMContentLoaded", function (event) {
        console.log("DOM fully loaded and parsed");
        init();
    });

})();