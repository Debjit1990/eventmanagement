
/**
 * Module dependencies.
 */

var express = require('express')
  , routes = require('./routes')
  , user = require('./routes/user')
  , http = require('http')
  , path = require('path')
  , eventMgmtService = require('./service/eventMgmt');;

var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

app.get('/', routes.index);
app.get('/users', user.list);
app.post('/rest/EmergencyContacts_track_view_details', eventMgmtService.EmergencyContacts_track_view_details);
app.post('/rest/updateEmergencyContactDetails', eventMgmtService.updateEmergencyContactDetails);
app.post('/rest/ReportIncident_list',eventMgmtService.ReportIncident_list);
app.post('/rest/updateIncident', incidentMgmtService.updateIncident);

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
