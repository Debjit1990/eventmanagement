/**
 * New node file
*/

var mongoose = require('mongoose');

//**mCproduction url**//
// mongoose.connect('mongodb://root:6TbWk9Lr0b9u@localhost');
//**end**//

//**Frproduction url**//
 // mongoose.connect('mongodb://root:tolJoHNZdey9@localhost');
//**end**//

// mongoose.connect('mongodb://localhost');
// mongoose.connect('mongodb://root:9SUHG7q343iP@localhost');
// var exotel = require('exotel-client');

//**testing server**//
 mongoose.connect('mongodb://root:cr67jQ2B2vOb@localhost');
//**end**//
var IncidentSchema = new mongoose.Schema({
	user_id:String,
	tokenid:{ type: String, required: true, index: {unique: true}, dropDups: true },
	category:String,
	type: String,
	reportedDateTime:Date,
	lastupdated_date:Date,
	emergency:Boolean,
	desc:String,
	locationLong:{type:String,required:true},
	locationLatd:{type:String,required:true},
	reportedUserID:String,
	address:{type:String,required:true},
	userType:String,
	alertType:String,
	statusbar:String,
	affectedPeopleCount: Number,
	reportsCount:Number,
	photoURL: String,
	updateincident_image:String,
	role:String,
	support_count1:String,
	like_count:String,
	feedback:String,
	regiid:String,
	// feedback:String,
	category_image:String,
	ReportBy:String,
	Incident_reported_to:String,
	main_category_name:String,
	loc: {
    type: [Number],  // [<longitude>, <latitude>]
    index: '2d'      // create the geospatial index
    },
    partner_rolecode_related_to_pitetions:String,
    petition_sub_section:String,
    reassign_petition_person:String,
    entity_name:String,
    district_name:String,

    level0_worker_1:String,
    level0_worker_1_name:String,
	level0_resolution_date1:Date,
	level0_remainder_date:Date,
	level0_auto_escalation_date:Date,
	level0_escalation_flag:String,
	level0_remainder_days:Number,
	level0_sla_days:Number,
	level0_worker_1_division_name:String,
	level0_worker_1_designation:String,
	level0_worker_1_supervisor_id:String,

	level0_worker_2:String,
	level0_worker_2_name:String,
	level0_worker_2_remainder_date:Date,
    level0_worker_2_auto_escalation_date:Date,
    level0_worker_2_escalation_flag:String,
    level0_worker_2_role:String,
    level0_worker_2_designation:String,
    level0_worker_2_division_name:String,
    level0_worker_2_supervisor_id:String,

    level0_worker_3:String,
    level0_worker_3_name:String,
    level0_worker_3_remainder_date:Date,
    level0_worker_3_auto_escalation_date:Date,
    level0_worker_3_escalation_flag:String,
    level0_worker_3_role:String,
    level0_worker_3_designation:String,
    level0_worker_3_division_name:String,
    level0_worker_3_supervisor_id:String,

    level0_worker_4:String,
    level0_worker_4_name:String,
    level0_worker_4_remainder_date:Date,
    level0_worker_4_auto_escalation_date:Date,
    level0_worker_4_escalation_flag:String,
    level0_worker_4_role:String,
    level0_worker_4_designation:String,
    level0_worker_4_division_name:String,
    level0_worker_4_supervisor_id:String,

    level0_worker_5:String,
    level0_worker_5_name:String,
    level0_worker_5_remainder_date:Date,
    level0_worker_5_auto_escalation_date:Date,
    level0_worker_5_escalation_flag:String,
    level0_worker_5_role:String,
    level0_worker_5_designation:String,
    level0_worker_5_division_name:String,
    level0_worker_5_supervisor_id:String,

    //Old Scheme//
 //    level0_worker_1:String,
	// level0_worker_2:String,
	// level0_worker_3:String,
	// level0_worker_4:String,
	// level0_worker_5:String,
	// level0_worker_2_remainder_date:Date,
 //    level0_worker_2_auto_escalation_date:Date,
 //    level0_worker_2_escalation_flag:String,
	// level0_resolution_date1:Date,
	// level0_remainder_date:Date,
	// level0_auto_escalation_date:Date,
	// level0_escalation_flag:String,
	// level0_remainder_days:Number,
	// level0_sla_days:Number,
 //    level0_worker_2_role:String,
 //    level0_worker_2_designation:String,
 //    level0_worker_2_division_name:String,
 //    level0_worker_2_name:String,
      //END//

	level1_worker_1:String,
	level1_worker_2:String,
	level1_worker_3:String,
	level1_worker_4:String,
	level1_worker_5:String,
	level1_resolution_date1:Date,
	level1_remainder_date:Date,
	level1_auto_escalation_date:Date,
	level1_escalation_flag:String,
	level1_remainder_days:Number,
	level1_sla_days:Number,

	level2_worker_1:String,
	level2_worker_2:String,
	level2_worker_3:String,
	level2_worker_4:String,
	level2_worker_5:String,
	level2_resolution_date1:Date,
	level2_remainder_date:String,
	level2_auto_escalation_date:String,
	level2_escalation_flag:String,
	level2_remainder_days:Number,
	level2_sla_days:Number,

	level3_worker_1:String,
	level3_worker_2:String,
	level3_worker_3:String,
	level3_worker_4:String,
	level3_worker_5:String,
	level3_resolution_date1:Date,
	level3_remainder_date:String,
	level3_auto_escalation_date:String,
	level3_escalation_flag:String,
	level3_remainder_days:Number,
	level3_sla_days:Number,

	level4_worker_1:String,
	level4_worker_2:String,
	level4_worker_3:String,
	level4_worker_4:String,
	level4_worker_5:String,
	level4_resolution_date1:Date,
	level4_remainder_date:String,
	level4_auto_escalation_date:String,
	level4_escalation_flag:String,
	level4_remainder_days:Number,
	level4_sla_days:Number,

	level5_worker_1:String,
	level5_resolution_date1:Date,
	level5_remainder_date:String,
	level5_auto_escalation_date:String,
	level5_escalation_flag:String,
	level5_remainder_days:Number,
	level5_sla_days:Number,
	updated_status:String,
});


var Incident = mongoose.model('Incident', IncidentSchema);

Incident.on('index', function(err) {
    console.log(err);
});
 // ObjectId = Incident.ObjectId;

var LocationDetailSchema = new mongoose.Schema({
	incidentId:String,
	timezone: String,
	titleOwnername: String,
	propertyName: String,
	houseNo: String,
	streetDetails: String,
	town: String,
	city: String,
	district: String,
	country: String,
	pincode: String
});

var Report_category_listSchema = new mongoose.Schema({
	category:String,
	rolecode: [{type:mongoose.Schema.Types.ObjectId,ref:'userrolemapping'}]
});

var ReportIncident_cat_list = mongoose.model('ReportIncident_cat_list', Report_category_listSchema);


var LocationDetail = mongoose.model('LocationDetail', LocationDetailSchema);


var UserDetailSchema = new mongoose.Schema({
	user_name:{type:String,required:true},
	user_mobile_number:{ type : String , unique : true, required : true, dropDups: true },
	user_password:{type:String,required:true},
	user_email:String,
	role:Array ,
	// roletype:String,
	rolecodes:String,
	orgcode:String,
	org_reg_id:String,
	regiid:String,
	status:String,
	donor_blood_group:String,
    last_donated_data:String,
    locationLong:String,
    locationLatd:String,
    category_image:String,
    loc: {
        type: [String],  // [<longitude>, <latitude>]
        index: '2d'      // create the geospatial index
        },
    address:String,
    address1:String,
    address2:String,
    dob:String,
    weight:String,
    flag:String,
    profilepicture:String,
    shoutout_status:String,
    sla_date:String,
    level:String,
    belongsto_escalated_level:String,
    is_it_supervisor:String,
    supervisor_name:String,
    supervisor_id:String,
    reminder_days:Number,
    entity_name:String,
    designation:String,
    belongs_to_section:String,
    Officer_division:String,
    district_name:String,
    Under_Maintanance:String,
});

var UserDetail = mongoose.model('UserDetail', UserDetailSchema);


var EmergencyContactDetailSchema = new mongoose.Schema({
    user_id:String,
	geoaddress:String,
	longitudevalue:String,
	lattitudevalue:String,
	primarycontact_name:String,
    primarycontact_mobileno:String,
	primarycontact_emailid:String,
    alternative_name:String,
    alternative_emailid:String,
	alternative_mobileno:String,
	reportedUserID :String
});

var EmergencyContactDetail = mongoose.model('EmergencyContactDetail', EmergencyContactDetailSchema);
var EcontactSchema=new mongoose.Schema({

geoaddress:String,
longitudevalue:String,
lattitudevalue:String,
primarycontact_name:String,
primarycontact_emailid:String,
primarycontact_mobileno:String,
alternative_name:String,
alternative_emailid:String,
alternative_mobileno:String,
reportedUserID :String,
location_address:String,
name:String,
email:String,
mobileno:String,
msg:String,
});
var Econtact=mongoose.model('Econtact',EcontactSchema);
var ContactusSchema=new mongoose.Schema({
name:String,
email:String,
mobileno:String,
msg:String,
});
var Contactus=mongoose.model('Contactus',ContactusSchema);


// var imagesSchema = new mongoose.Schema({
// 	images: String,
// 	// tokenid: String,
// })
// var images = mongoose.model('images',imagesSchema);

var Incident_social_contributionSchema=new mongoose.Schema({
user_id:String,
token_id:String,
is_like:String,
is_support:String,

});
var Incident_social_contribution=mongoose.model('Incident_social_contribution',Incident_social_contributionSchema);

var userrolemappingschema = new mongoose.Schema({
	rolecode: {type: String, unique: true},
	roledescription:String,
	roletypegovt:String,
	roletypeprivate:String,
	userType:String,
	district_name:String,
	entity_name:String,
});

// [{type: mongoose.Schema.Types.ObjectId, ref: 'UserModel'}]

var userrolemapping = mongoose.model('userrolemapping',userrolemappingschema);

var userrolecode_addcategoryschema = new mongoose.Schema({
	cat_list:{type:String,required : true},
	user_role_code:{type:String,required : true},
	usertype:String,
	imageurl:String,
	priority:Number,
	autoallocation:String,
	entity_name:String,
	district_name:String,
})

var user_rolecode_addcategory = mongoose.model('user_rolecode_addcategory',userrolecode_addcategoryschema);


var organizationaddschema = new mongoose.Schema({
	orgname:{type:String},
	orgcode:{type:String},
	orgtype:{type:String},
})

var organizationsetup = mongoose.model('organizationsetup',organizationaddschema);


var incident_type_second_levelschema = new mongoose.Schema({
	cat_name : {type:String,required : true},
	cat_type : {type:String,required : true},
	userrole : {type:String,required : true},
	usertype : {type:String,required : true},
	imageurl : {type:String,required : true},
	section  :String,
	entity_name:String,
	district_name:String,
})

var incidentlist_second_level =  mongoose.model('incidentlist_second_level',incident_type_second_levelschema);


var incident_type_third_levelschema = new mongoose.Schema({
	cat_name : {type:String,required : true},
	cat_type : {type:String,required : true},
	userrole : {type:String,required : true},
	usertype : {type:String,required : true},
	imageurl : {type:String,required : true},
	section  : {type:String,required : true},
	section_name:String,
	entity_name:String,
	district_name:String,
})

var incidentlist_third_level =  mongoose.model('incidentlist_third_level',incident_type_third_levelschema);

var blood_serviceschema = new mongoose.Schema({
	list_name : {type:String,required : true},
//	list_type : {type:String,required : true},
//	userrole : {type:String,required : true},
//	usertype : {type:String,required : true},
	imageurl : {type:String,required : true},
})
var blood_service =  mongoose.model('blood_service',blood_serviceschema);


var donor_registrationschema=new mongoose.Schema({
donro_name:String,
donor_mobileno:String,
donor_email:String,
donor_blood_group:String,
last_donated_data:String,
dob:String,
weight:Number,
})
var donor_registration=mongoose.model('donor_registration',donor_registrationschema);


var request_for_bloodschema=new mongoose.Schema({
donor_name:String,
donor_mobileno:String,
donor_email:String,
donor_blood_group:String,
last_donated_date:String,
need_description:String,
who_requests:String,
list_id:String,
req_name:String,
time:Date,
requirement_date:String,
status:String,
noofdonorsinterest:String,
})
var request_for_blood=mongoose.model('request_for_blood',request_for_bloodschema);


var emergency_tracking_personSchema = new mongoose.Schema({
emergency_person_name:String,
picture:String,
emergency_person_id:String,
lattitudevalue:String,
longitudevalue:String,
location_address:String,
primarycontact_mobileno:String,
alternative_mobileno:String,
POR1:String,
POR2:String,
POR3:String,
tracking_status:String,
avialable_track_status:String,
track_date:Date,
})

var emergency_person_tracking = mongoose.model('emergency_person_tracking',emergency_tracking_personSchema);


// var emergency_person_tracking_PORSchema = new mongoose.Schema({
// 	victim_id:String,
// 	POR:String,
// 	watching_status:String,
// 	watching_date:Date,
// })

// var emergency_person_tracking_POR = mongoose.model('emergency_person_tracking_POR',emergency_person_tracking_PORSchema);


var donor_interestSchema = new mongoose.Schema({
list_id:String,
flag:String,
donor_mobileno:String,

})
var donor_interest = mongoose.model('donor_interest',donor_interestSchema);

var incident_commentsSchema = new mongoose.Schema({
user_name:String,
user_mobileno:String,
tokenid:String,
time:Date,
role_type:String,
status:String,
comments:String,
})

var incident_comments = mongoose.model('incident_comments',incident_commentsSchema);


var bloodbankcontactsschema=new mongoose.Schema({
		blood_bank_name:String,
		blood_bank_mobileno:String,
		})
var bloodbankcontacts=mongoose.model('bloodbankcontacts',bloodbankcontactsschema);

var shoutoutpartnersschema=new mongoose.Schema({
partner_name:String,
partner_mobileno:String,
partner_email:String,
})
var shoutoutpartners=mongoose.model('shoutoutpartners',shoutoutpartnersschema);

var shoutout_sms_email_statusschema=new mongoose.Schema({
sms_status:String,
email_status:String,
})
var shoutout_sms_email_status=mongoose.model('shoutout_sms_email_status',shoutout_sms_email_statusschema);


var DivisionSchema = new mongoose.Schema({
	divisionname:String,
	divisionvalue:String,
	district_name:String,
	state_name:String,
});

var Division = mongoose.model('Division', DivisionSchema);

// var PetitionSchema = new mongoose.Schema({
// 	user_id:String,
// 	tokenid:{ type: String, required: true, index: {unique: true}, dropDups: true },
// 	category:String,
// 	sectionname: String,
// 	reportedDateTime:Date,
// 	lastupdated_date:Date,
// 	desc:String,
// 	userType:String,
// 	statusbar:String,
// 	reported_photoURL: String,
// 	updated_photoURL:String,
// 	role:String,
// 	regiid:String,
// 	category_image:String,
// 	Petition_reported_to:String,
// 	main_category_name:String,
// });

// var Petition = mongoose.model('Petition', PetitionSchema);

// exports.emergency_person_tracking_POR = emergency_person_tracking_POR;
//exports.Petition = Petition;
exports.Division = Division;
exports.incident_comments = incident_comments;
exports.donor_interest=donor_interest;
exports.emergency_person_tracking = emergency_person_tracking;
exports.request_for_blood=request_for_blood;
exports.donor_registration=donor_registration;
exports.blood_service = blood_service;
exports.incidentlist_second_level = incidentlist_second_level;
exports.incidentlist_third_level = incidentlist_third_level;
exports.organizationsetup = organizationsetup;
exports.user_rolecode_addcategory = user_rolecode_addcategory;
exports.userrolemapping = userrolemapping;
exports.Incident = Incident;
exports.LocationDetail = LocationDetail;
exports.UserDetail = UserDetail;
exports.EmergencyContactDetail = EmergencyContactDetail;
exports.Econtact=Econtact;
exports.Contactus=Contactus;
exports.ReportIncident_cat_list = ReportIncident_cat_list;
exports.Incident_social_contribution=Incident_social_contribution;
exports.shoutout_sms_email_status = shoutout_sms_email_status;
exports.shoutoutpartners = shoutoutpartners;
exports.bloodbankcontacts = bloodbankcontacts;
