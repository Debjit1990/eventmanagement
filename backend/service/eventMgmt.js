/**
 * New node file
 */
var path = require('path'),
    fs = require('fs');

var util = require('util');

/*
exports.pushnotifications = function (req, res) {
	var eventMgmtDao = require(process.cwd() + '/dao/eventMgmtDao');
	//console.log('updateIncident called: ' + req);
	eventMgmtDao.pushnotifications(req, res);
}
*/

exports.EmergencyContacts_track_view_details = function (req, res) {
	var eventMgmtDao = require(process.cwd() + '/dao/eventMgmtDao');
	console.log('EmergencyContacts_track_view_details called: ' + req);
	eventMgmtDao.EmergencyContacts_track_view_details(req, res);
}
//*end of Push notification functions*//



exports.updateEmergencyContactDetails = function (req, res) {
	var eventMgmtDao = require(process.cwd() + '/dao/eventMgmtDao');
	//console.log('updateIncident called: ' + req);
	eventMgmtDao.updateEmergencyContactDetails(req, res);
};

exports.ReportIncident_list = function (req, res) {

	var eventMgmtDao = require(process.cwd() + '/dao/eventMgmtDao');
	console.log('ReportIncident_list called: ' + req);
	eventMgmtDao.ReportIncident_list(req, res);
};

exports.ReportIncident_list_get = function (req, res) {
	var eventMgmtDao = require(process.cwd() + '/dao/eventMgmtDao');
	console.log('ReportIncident_list_get called: ' + req);
	eventMgmtDao.ReportIncident_list_get(req, res);
};



exports.updateIncident = function (req, res) {
	var eventMgmtDao = require(process.cwd() + '/dao/eventMgmtDao');
	//console.log('updateIncident called: ' + req);
	eventMgmtDao.updateIncident(req, res);
};

exports.addEmergencyContact = function (req, res) {
	var eventMgmtDao = require(process.cwd() + '/dao/eventMgmtDao');
	console.log('addEmergencyContact called: ' + req);
	eventMgmtDao.addEmergencyContact(req, res);
};

